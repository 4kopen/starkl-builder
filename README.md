##Possible command modifiers
-e TARGET=${} changes the starkl to build example:
```bash
docker run --cap-add=SYS_PTRACE --privileged=true -e TARGET=starkl-b2264-default -it --rm -v "$(pwd)":/starkl 4kopen/starkl-builder:latest
```
-e ADDITIONAL=${tar|uenv} creates either a uenv OR a tar containing a uenv.txt and kernel.fit
NOTE: Not available for raspbian builds.
```Bash
docker run --cap-add=SYS_PTRACE --privileged=true -e ADDITIONAL=tar -it --rm -v "$(pwd)":/starkl 4kopen/starkl-builder:latest
```
## Build

```bash
docker build -t starkl_build .
```

## Run (Bash)

```bash
docker run -it --rm -v "$(pwd)":/starkl -u starkl starkl_build bash
```

## Run (Build)

```bash
docker run --cap-add=SYS_PTRACE --privileged=true -it --rm -v "$(pwd)":/starkl 4kopen/starkl-builder:latest
```


# MacOS Instructions for running docker

## First edit /etc/nfs.conf and add the following line
```
nfs.server.mount.require_resv_port = 0
```

## Download nfsmanager or edit /etc/exports and add a line
```
/Volumes/Location/of/Starkl -alldirs -mapall=501 # Replace 501 with your userid
```

## Restart the nfs daemon
```
sudo nfsd restart
sudo nfsd checkexports
```

## Volumes have to be created first
```
sudo docker volume create --driver local --opt type=nfs --opt o="vers=3,addr=<macos-server-ip>,nolock,rw" --opt device=":/Volumes/Location/of/Starkl" nfs-starkl
```

## Run the Build
```
docker run -it --rm --privileged=true -v nfs-starkl:/starkl <id>
```

## Running from MacOS
```
docker run --rm --cap-add=SYS_PTRACE --privileged=true -v nfs-starkl:/starkl -it <id> /starkl/launchers/stmc2/launch-starkl /starkl/outputs/starkl-b2264-default/images 10.0.1.180 starkl=ip:eth0:10.0.0.62/16:10.0.0.1
```

# ref: https://docs.docker.com/engine/reference/builder/
FROM ubuntu:16.04

ARG DEBIAN_FRONTEND=noninteractive

# chooses which path to build along
ENV TARGET starkl-b2264-default
# chooses between: none (no additional), uenv (creates a uenv.txt), tar (creates a uenv.txt, and then tars uenv.txt and kernel.fit into a tar file)
ENV ADDITIONAL none

# prepare the system
RUN apt-get update \
	&& apt-get upgrade -y

RUN apt install -y \
	make wget cpio python git subversion bzip2 flex bison m4 \
	gperf telnet file rsync bc unzip build-essential g++ intltool \
	libxml-tokeparser-perl binwalk u-boot-tools device-tree-compiler mercurial libncurses5-dev

RUN groupadd -g 1000 starkl \
	&& useradd -g 1000 -u 1000 -s /bin/bash -md /starkl starkl

VOLUME /starkl

# MacOS Instructions for docker (not sure if this works with boot2docker)
#
# Remember to run with --privileged=true and create a volume like so:
#
# sudo docker volume create --driver local --opt type=nfs --opt o="vers=3,addr=192.168.63.1,uid=1000,gid=1000,rw" --opt device=":/Volumes/Extra/top-level/me/projects/4kopen/bitbucket/s" nfs-starkl
#
# docker run -it -v nfs-starkl:/starkl --rm <id>

CMD cd /starkl \
	&& apt install -y $(su starkl -c "make list-required-debian-packages") \
	&& su starkl -c 'umask 0022; make outputs/"$TARGET"/images' \
	&& su starkl -c 'chmod 777 ./make_fit_image.sh && ./make_fit_image.sh "$TARGET" "$ADDITIONAL"' \
	&& su starkl -c 'echo "The software which has been built is bound by a number of licenses. Please consult the LICENSE and/or COPYING file of each package for details. I acknowledge that by using any of the software I am agreeing to the terms of the applicable licenses."'
